import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import questions from '../components/questions.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/quiz/:themeId',
    name: 'questions',
    component: questions,
    props: true
  },
]

const router = new VueRouter({
  routes
})

export default router
