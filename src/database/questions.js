export default {
    questionsDB: [
        { 
            themeName: "Dogs 🐕",
            id: 1,
            afooter: 'Based on Turid Rugaas Book On talking terms with dogs. You can also check <a href ="http://en.turid-rugaas.no/" style="a: {color: goldenrod}">her website</a>',
            questions: [
                {
                    question: "When a dog turns his back on a strange person or dog, what could it mean?",
                    options: ["the dog is upset with the person/dog",
                        "the dog is not interested in any interaction",
                        "the dog is trying to communicate that he is not happy with this dog/person's approach on him"],
                    correctAnswer: 2
                },
                {
                    question: "When a dog is yawning, what could it mean?",
                    options: ["the dog is angry",
                        "the dog is sleepy or tired",
                        "the dog is unconfortable"],
                    correctAnswer: 2
                },
                {
                    question: "When in a presence of another dog, if your dog sniffs the ground, what could it mean?",
                    options: ["the dog is signaling that the is not looking for conflict",
                        "the dog smelled something",
                        "the dog is  not interested in the other dog."],
                    correctAnswer: 0
                }
                
            ]
        },
        { 
            themeName: "The legend of Zelda Games ⚔️",
            id: 2,
            afooter: "Play the Legend of Zelda Games!",
            questions: [
                {
                    question: "What type of animal is Epona?",
                    options: ["a fish",
                        "an owl",
                        "a horse"],
                    correctAnswer: 2
                },
                {
                    question: "What do gorons eat?",
                    options: ["fire and lava",
                        "stones and rocks",
                        "birds and insects"],
                    correctAnswer: 1
                },
                {
                    question: "How much time is there in the 'Majora's Mask' Game before the end of the world?",
                    options: ["2 months",
                        "72 hours",
                        "4 days"],
                    correctAnswer: 1
                }
            ]
        }
    ]
}
