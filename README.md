# quiz
I started this project based on [scrimba's weekley web dev challenge](https://scrimba.com/learn/weeklychallenge). And while doing it I tought it could be useful for people teaching online and in deed to do a quiz or some kind of test online. I create two themes with 3 questions each. You can create as many questions and themes as you would like.

To create your own quiz, all you have to do is edit the _questions.js_ file.
Soon, I will create a form for people afraid of coding to use this as well (there is a commented button "create a quiz" that will serve this purpose).




## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
